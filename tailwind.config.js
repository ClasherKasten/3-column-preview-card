module.exports = {
	content: ["./src/**/*.{html,js,css}"],
	theme: {
		extend: {
			fontFamily: {
				"rpc-ld": ["Lexend Deca"],
				"rpc-bsd": ["Big Shoulders Display"]
			},
			colors: {
				"prc-bright-orange": "hsl(31, 77%, 52%)",
				"prc-dark-cyan": "hsl(184, 100%, 22%)",
				"prc-very-dark-cyan": "hsl(179, 100%, 13%)",
				"prc-transparent-white": "hsla(0, 0%, 100%, 0.75)",
				"prc-very-light-gray": "hsl(0, 0%, 95%)",
			}
		},
	},
	plugins: [],
}
